#!/usr/bin/node

const acorn = require("acorn")
const walk = require("acorn/dist/walk")

walk.simple(acorn.parse("let x = 10"), {
  Literal(node) {
    console.log(`Found a literal: ${node.value}`)
  }
})
